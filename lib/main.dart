import 'package:danilomobile/app.dart';
import 'package:flutter/material.dart';

void main() async {
  // ensure all of the flutter
  // widget and all component initialize before we use it
  WidgetsFlutterBinding.ensureInitialized();

  // run the application
  // define all of the theme, routes, setting
  runApp(const DaniloApp());
}
