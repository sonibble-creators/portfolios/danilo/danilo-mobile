import 'package:danilomobile/screens/splash.dart';
import 'package:flutter/material.dart';

class DaniloApp extends StatelessWidget {
  const DaniloApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Danilo',
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.system,
      theme: ThemeData(useMaterial3: true),
      home: const Splash(),
    );
  }
}
