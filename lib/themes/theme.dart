import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

abstract class CustomColors {
  const CustomColors();
  CustomColors lerp(CustomColors other, double t);
}

class MyCustomColors implements CustomColors {
  const MyCustomColors({this.brandRed, required this.brandBlue});

  final Color? brandRed;
  final Color? brandBlue;

  @override
  CustomColors lerp(CustomColors other, double t) {
    assert(other is MyCustomColors);
    if (other is MyCustomColors) {
      return MyCustomColors(
        brandRed: Color.lerp(brandRed, other.brandRed, t),
        brandBlue: Color.lerp(brandBlue, other.brandBlue, t),
      );
    }
    return this;
  }
}
